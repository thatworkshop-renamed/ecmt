var webpack = require("webpack");

module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      app: ['dist']
    },
    copy: {
      app: {
        files: [
          {flatten: true, expand: true, src: ['src/app/index.html'], dest: 'dist/public'},
          {flatten: false, expand: true, cwd: 'src/app/img', src: ['**/*'], dest: 'dist/public/img'},
          {flatten: false, expand: true, cwd: 'src/app/lib', src: ['**/*'], dest: 'dist/public/lib'}
        ]
      }
    },
    webpack: {
      options: {
        entry: './src/app/main.jsx',
        output: {
          path: './dist/public/js',
          filename: 'app.js',
          sourceMapFilename: '[file]_[hash].map'
        },
        devtool: 'source-map',
        module: {
          loaders: [
            {
              test: /\.js/,
              exclude: /node_modules/,
              loader: 'babel',
              query: {
                presets: ['es2015', 'react', 'stage-2']
              }
            }
          ]
        }
      },
      build: {
        plugins: [].concat(
          new webpack.DefinePlugin({
						"process.env": {
							// This has effect on the react lib size
							"NODE_ENV": JSON.stringify("development")
						}
					})
        )
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'dist/public/css/app.min.css': 'src/app/scss/main.scss'
        }
      }
    },
    watch: {
      app: {
        files: ['src/app/**/*'],
        tasks: ['build'],
        options: {
          spawn: false
        }
      }
    }
  });

  grunt.registerTask('default', ['build', 'watch']);
  grunt.registerTask('build', ['clean:app', 'webpack:build', 'sass', 'copy:app']);
};
