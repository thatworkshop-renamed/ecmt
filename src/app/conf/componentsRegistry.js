const componentsRegistry = {
  '/layouts/Home': ['/layout'],
  '/SignIn': ['/home'],
  '/WelcomeScreen': ['**/welcome'],
  '/Copy': ['**/copy'],
  '/ApplyScreen': ['**/apply'],
  '/Home/Logo': ['/home/apply'],
  '/Navigation': ['**/back-home']
};

const components = {};

const modules = require.context('../components', true, /\.jsx$/);

modules.keys().forEach(moduleName => {
  const component = moduleName.replace('./', '/').replace('.jsx', '');

  components[component] = {
    module: modules(moduleName).default,
    contexts: componentsRegistry[component]
  };
});

export default components;
