import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

import store from './store';
import Application from './containers/Application.jsx';
import {bootstrap} from './actions';

render(
  <Provider store={store}>
    <Application />
  </Provider>,
  document.getElementById('app')
);

store.dispatch(bootstrap());
