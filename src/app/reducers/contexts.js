import multimatch from 'multimatch';

import * as context from './../helpers/context.js';

const contexts = context.createReducer({}, {
  ['REVOKE_CONTEXT']: (state, {context}) => {
    const newState = {...state};

    delete newState[context];

    return newState;
  },

  ['INVOKE_CONTEXT']: (state, {context, payload, override}) => {
    let newState;

    if (context.indexOf('FETCH_SUCCESS') > 0 && state[context.split('/FETCH_SUCCESS')[0]]) {
      context = context.split('/FETCH_SUCCESS')[0];
      override = state[context].override;

      newState = {...state, [context]: {payload, override}};

      delete newState[context + '/FETCH_START'];

      return newState;
    } else {
      newState = {...state, [context]: {payload, override}};

      return newState;
    }
  }
});

export default contexts;
