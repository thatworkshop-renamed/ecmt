import componentsRegistry from '../conf/componentsRegistry.js';

const components = (state = componentsRegistry) => {
  return state;
};

export default components;
