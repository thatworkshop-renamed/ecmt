import * as context from './../helpers/context.js';

const queue = context.createReducer([], {
  ['CONTEXT_QUEUE_UPDATE']: (state, {queue}) => {
    return [...queue];
  }
});

export default queue;
