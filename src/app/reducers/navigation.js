/**
 * Mock data used for demonstrational purposes only. Should be removed
 * once appropriate sections are added.
 */
const navigationState = {
  items: [
    {
      label: 'Events',
      url: '/events',
      context: 'events.browse'
    },
    {
      label: 'Venues',
      url: '/venues',
      context: 'venues.browse'
    }
  ],
  brand: {
    url: '/',
    title: 'Tickets',
    context: 'default'
  }
};

const navigation = (state = navigationState, action) => {
  switch (action.type) {
    case 'ROUTER_INIT':
      navigationState.items = navigationState.items.map(item => {
        item.url = action.router.url(item.context);
        item.onClick = action.router.navigate.bind(null, item.context);

        return item;
      });

      return {...state, navigationState};
    default:
      return state;
  }
};

export default navigation;
