import {createReducer} from './../helpers/reducer.js';

const initialState = {
  currentRoute: null,
  navigate: () => {},
  url: () => {}
};

const router = createReducer(initialState, {
    ['ROUTER_INIT']: (state, {router}) => ({...state, ...router})
});

export default router;
