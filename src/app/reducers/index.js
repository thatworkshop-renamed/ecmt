import router from './router.js';
import contexts from './contexts.js';
import queue from './queue.js';
import components from './components.js';

import * as conf from './../conf';

// TODO: think about moving contexts configuration to a separate reducer
export default {__contexts: () => conf.contexts, queue, router, contexts, components};
