import {createStore, combineReducers, applyMiddleware} from 'redux';

import reducers from './../reducers';
import middleware from './../middleware';

const store = createStore(combineReducers(reducers), applyMiddleware(...middleware));

export default store;
