import multimatch from 'multimatch';

import {invokeContext} from './../actions';

const contextResources = {
  '**/welcome': {
    GET: '/api/static/welcome'
  }
};

const data = store => next => action => {
  if (action.type === 'INVOKE_CONTEXT') {
    Object.keys(contextResources).forEach(context => {
      const matches = multimatch(action.context, context);

      if (matches.length) {
        store.dispatch(invokeContext(action.context + '/FETCH_START'));

        fetch(contextResources[context].GET).then(res => {
          res.json().then(payload => store.dispatch(invokeContext(action.context + '/FETCH_SUCCESS', payload)));
        });
      }
    });
  }

  return next(action);
};

export default data;
