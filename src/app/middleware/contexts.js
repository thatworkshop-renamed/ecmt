import multimatch from 'multimatch';

import {revokeContext, invokeContext, contextQueueUpdate} from './../actions';

import * as conf from './../conf';

const contextMap = {};

const getTargetPaths = () => {
  return conf.contexts.map((rule, index, rules) => {
    let [, parentContext, operation, targetContext] = rule.match(/^(\/[a-z, \-, \/, *]*)\s([\+, \-, \?]+)\s([\., \/, \*, \-, a-z]*)/);

    return (targetContext.indexOf('/') === 0 ? targetContext : parentContext + '/' + targetContext).replace('//', '/');
  });
};

const contexts = store => next => action => {
  const revokeContexts = context => {
    let invokedContexts = Object.keys(store.getState().contexts);

    conf.contexts.forEach((rule, index, rules) => {
      const [, parentContext, operation, targetContext] = rule.match(/^(\/[a-z, \-, \/, *]*)\s([\+, \-, \?]+)\s([\., \/, \*, \-, a-z]*)/);

      let targetContextPath = (targetContext.indexOf('/') === 0 ? targetContext : parentContext + '/' + targetContext).replace('//', '/');

      if (operation === '-' && multimatch(context, parentContext).length) {
        multimatch(invokedContexts, targetContextPath).forEach(invokedContext => {
          if (invokedContext !== context) {
            store.dispatch(revokeContext(invokedContext));
          }
        });
      }
    });
  };

  if (action.type === 'BOOTSTRAP') {
    store.dispatch(invokeContext('/'));

    return next(action);
  }

  if (action.type === 'REVOKE_CONTEXT') {
    let invokedContexts = Object.keys(store.getState().contexts);

    conf.contexts.forEach((rule, index, rules) => {
      const [, parentContext, operation, targetContext] = rule.match(/^(\/[a-z, \-, \/, *]*)\s([\+, \-, \?]+)\s([\., \/, \*, \-, a-z]*)/);

      const targetContextPath = (targetContext.indexOf('/') === 0 ? targetContext : parentContext + '/' + targetContext).replace('//', '/');

      if ((operation === '+' || operation === '+?') && multimatch(action.context, parentContext).length) {
        multimatch(invokedContexts, targetContextPath).forEach(context => {
          if (context !== action.context) {
            store.dispatch(revokeContext(context));
          }
        });
      }
    });
  }

  if (action.type === 'INVOKE_CONTEXT') {
    let invokedContexts = Object.keys(store.getState().contexts);

    // Skip already invoked contexts.
    if (invokedContexts.indexOf(action.context) >= 0) {
      next(action);
    }

    if (action.context === '/' || action.context.indexOf('FETCH_') >= 0) {
      next(action);
    }

    invokedContexts = Object.keys(store.getState().contexts);

    // Invoke parent contexts.
    conf.contexts.forEach((rule, index, rules) => {
      const [, parentContext, operation, targetContext] = rule.match(/^(\/[a-z, \-, \/, *]*)\s([\+, \-, \?]+)\s([\., \/, \*, \-, a-z]*)/);

      let targetContextPath = (targetContext.indexOf('/') === 0 ? targetContext : parentContext + '/' + targetContext).replace('//', '/');

      if ((operation === '+' || operation === '+?') && multimatch(action.context, targetContextPath).length) {
        if (!multimatch(invokedContexts, parentContext).length) {
          store.dispatch(invokeContext(parentContext));
        } else {
          revokeContexts(parentContext);
        }
      }
    });

    // Revoke contexts.
    conf.contexts.forEach((rule, index, rules) => {
      const [, parentContext, operation, targetContext] = rule.match(/^(\/[a-z, \-, \/, *]*)\s([\+, \-, \?]+)\s([\., \/, \*, \-, a-z]*)/);

      let targetContextPath = (targetContext.indexOf('/') === 0 ? targetContext : parentContext + '/' + targetContext).replace('//', '/');

      if (operation === '-' && multimatch(action.context, parentContext).length) {
        multimatch(invokedContexts, targetContextPath).forEach(context => {
          if (context !== action.context) {
            store.dispatch(revokeContext(context));
          }
        });
      }
    });

    if (invokedContexts.indexOf(action.context) < 0 && action.context.indexOf('FETCH_') < 0) {
      next(action);
    }

    invokedContexts = Object.keys(store.getState().contexts);

    // Invoke child contexts.
    conf.contexts.forEach((rule, index, rules) => {
      const [, parentContext, operation, targetContext] = rule.match(/^(\/[a-z, \-, \/, *]*)\s([\+, \-, \?]+)\s([\., \/, \*, \-, a-z]*)/);

      let targetContextPath = (targetContext.indexOf('/') === 0 ? targetContext : parentContext + '/' + targetContext).replace('//', '/');

      if (operation === '+' && multimatch(action.context, parentContext).length) {
        if (targetContext.indexOf('/') < 0) {
          targetContextPath = (action.context + '/' + targetContext).replace('//', '/');
        }

        if (invokedContexts.indexOf(targetContextPath) < 0) {
          store.dispatch(invokeContext(targetContextPath));
        }
      }
    });
  } else {
    return next(action);
  }
};

export default contexts;
