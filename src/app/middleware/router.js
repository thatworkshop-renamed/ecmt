import routes from './../routes';
import {invokeContext, routerInit} from './../actions';

// Given a route, and a URL fragment that it matches, return the array of
// extracted decoded parameters. Empty or unmatched parameters will be
// treated as `null` to normalize cross-browser behavior.
const extractParameters = (route, fragment) => {
  let params = route.regExp.exec(fragment).slice(1);

  const values = params.map((param, i) => {
    // Don't decode the search params.
    if (i === params.length - 1) return param || null;

    return param ? decodeURIComponent(param) : null;
  });

  const paramsMap = {};

  let names = /(:\w+)/g.exec(route.path) || [];

  if (names) {
    names = names.slice(1).map(match => {
      const prop = match.replace(':', '');

      return prop || null;
    });
  }

  values.forEach((value, index) => {
    if (names[index]) {
      paramsMap[names[index]] = value;
    }
  });

  return paramsMap;
}

const router = store => next => action => {
  const currentPath = window.location.pathname !== '/' ? window.location.pathname.replace(/\/$/, '') : window.location.pathname;

  switch (action.type) {
    case 'BOOTSTRAP':
      window.addEventListener('popstate', () => {
        const currentPath = window.location.pathname !== '/' ? window.location.pathname.replace(/\/$/, '') : window.location.pathname;
        
        let payload = null;

        routes.forEach((route, context) => {
          if (route.regExp.test(currentPath)) {
            currentRoute.path = route.path;
            currentRoute.context = context;
            currentRoute.regExp = route.regExp;

            if (window.location.pathname !== '/') {
              payload = extractParameters(currentRoute, currentPath);
            }

            store.dispatch(invokeContext(context, payload));
          }
        });
      });

      next(action);

      const currentRoute = {
        context: store.getState().context,
        path: store.getState().router.path
      };

      let payload = null;

      routes.forEach((route, context) => {
        if (route.regExp.test(currentPath)) {
          currentRoute.path = route.path;
          currentRoute.context = context;
          currentRoute.regExp = route.regExp;

          if (window.location.pathname !== '/') {
            payload = extractParameters(currentRoute, currentPath);
          }
        }
      });

      store.dispatch(routerInit({
        currentRoute,
        navigate: (context, payload) => {
          store.dispatch(invokeContext(context, payload));
        },
        url: (context, params = null) => {
          let url = '/';

          if (routes.get(context)) {
            if (params) {
              url = routes.get(context).path.replace(/(:\w+)/g, (match, number) => {
                const prop = match.replace(':', '');

                return params[prop] || null;
              });
            } else {
              url = routes.get(context).path;
            }
          }

          return url;
        }
      }));

      if (currentRoute.context) {
        store.dispatch(invokeContext(currentRoute.context, payload));
      }

      break;
    case 'INVOKE_CONTEXT':
      const targetContext = (action.currentContext ? action.currentContext + '/' : '') + action.context;

      if (routes.has(targetContext)) {
        const path = routes.get(targetContext).path.replace(/(:\w+)/g, (match, number) => {
          const prop = match.replace(':', '');

          return action.payload[prop] || match;
        });

        if (path !== currentPath) {
          window.history.pushState(null, null, path);
        }
      }
    default:
      return next(action);
  }
}

export default router;
