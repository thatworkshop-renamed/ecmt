import log from './../helpers/log.js';

const logger = store => next => action => {
  let message;

  switch (action.type) {
    case 'INVOKE_CONTEXT':
      message = '[c="color: green; font-weight: bold;"]INVOKING CONTEXT:[c] [c="color: gray;"]' + action.context + '[c]';

      break;
    case 'REVOKE_CONTEXT':
      message = '[c="color: red; font-weight: bold;"]REVOKING CONTEXT:[c] [c="color: gray;"]' + action.context + '[c]';

      break;
    default:
      message = '[c="font-weight: bold;"]DISPATCHING ACTION:[c] [c="color: gray;"]' + action.type + '[c]';
  }

  log(message, {
    action: action,
    currentState: store.getState()
  });

  return next(action);
};

export default logger;
