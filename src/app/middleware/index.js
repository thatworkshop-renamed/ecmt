import router from './router.js';
import contexts from './contexts.js';
import data from './data.js';
import logger from './logger';

export default [router, contexts, data, logger];
