import React from 'react';

import ButtonWarning from './ButtonWarning.jsx';

import AbstractComponent from './AbstractComponent.jsx';

class Navigation extends AbstractComponent {
  back() {
    this.props.router.navigate('/content/home/welcome');
  }

  render() {
    return (
      <div className="navigation">
        <img src="img/logo.svg" alt="That Workshop Corporation" />
        <h1>That Workshop</h1>
        <ButtonWarning action={this.back.bind(this)}>Back home</ButtonWarning>
      </div>
    );
  }
}

export default Navigation;
