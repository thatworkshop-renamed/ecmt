import React from 'react';

class ButtonWarning extends React.Component {
  render() {
    return (
      <button
        type="button"
        className={['btn btn-warning', this.props.className].join(' ')}
        onClick={this.props.action}
      >
        {this.props.children}
      </button>
    );
  }
};

export default ButtonWarning;
