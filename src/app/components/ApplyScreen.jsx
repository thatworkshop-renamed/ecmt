import React from 'react';

import AbstractComponent from './AbstractComponent.jsx';

import ButtonPrimary from './ButtonPrimary.jsx';

class ApplyScreen extends AbstractComponent {
  render() {
    return (
      <div>
        <div className="col-lg-2 col-lg-offset-1">
        </div>
        <div className="col-lg-6">
          <div>
            <a>Authenticate</a>
            <a>Submit Application</a>
            <a>Success</a>
          </div>
          <p>In order to proceed you should authenticate your character.</p>
          <ButtonPrimary className="btn-lg">Authenticate</ButtonPrimary>
        </div>
      </div>
    );
  }
}

export default ApplyScreen;
