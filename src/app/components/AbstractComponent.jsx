import uuid from 'uuid';
import multimatch from 'multimatch';
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {invokeContext} from './../actions';

class AbstractComponent extends React.Component {
  constructor(...args) {
    super(...args);
  }

  willLeave() {
    console.log(this);
  }

  resolve(targetContext) {
    if (!targetContext || !this.props.components) return targetContext;

    if (targetContext.indexOf('/') !== 0) {
      targetContext = this.props.currentContext + '/' + targetContext;
    }

    const componentContexts = multimatch(Object.keys(this.props.contexts), targetContext);

    if (!componentContexts.length) return null;

    const components = [];

    componentContexts.forEach(context => {
      Object.keys(this.props.components).filter(component => {
        return multimatch(context, this.props.components[component].contexts).length;
      }).map((component, key) => {
        const Component = this.props.components[component].module;
        const payload = (this.props.contexts[context] ? this.props.contexts[context].payload : {}) || {};

        components.push(<Component currentContext={context} {...payload} key={component + '_' + key} router={this.props.router} contexts={this.props.contexts} components={this.props.components} invokeContext={this.props.invokeContext} />);
      });
    });

    return components;
  }
}

AbstractComponent.defaultProps = {
  currentContext: ''
};

export default AbstractComponent;
