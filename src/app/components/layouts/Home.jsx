import React from 'react';
import Transition from 'react-addons-css-transition-group';

import ButtonPrimary from './../ButtonPrimary.jsx';

import AbstractComponent from './../AbstractComponent.jsx';

class HomeLayout extends AbstractComponent {
  signIn() {
    this.props.invokeContext('/content/inner/apply');
  }

  render() {
    return (
      <div>
        <div className="conteiner-fluid page-home">
          <div className="row header">
            <div className="col-lg-6">
              {this.resolve('/content/inner/*/back-home')}
            </div>
            <div className="col-lg-6">
              <div className="pull-right signin">
                <p>Already a member?</p>
                <ButtonPrimary action={this.signIn.bind(this)}>Sing In</ButtonPrimary>
              </div>
            </div>
          </div>
          <div className="row body page-block">
            {this.resolve('/content/*/*')}
          </div>
          <div className="row footer">
            <div className="col-lg-6 col-lg-offset-3">
              {this.resolve('/copy')}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HomeLayout;
