import React from 'react';

class ButtonPrimary extends React.Component {
  render() {
    return (
      <button
        type="button"
        className={['btn btn-primary', this.props.className].join(' ')}
        onClick={this.props.action}
      >
        {this.props.children}
      </button>
    );
  }
};

export default ButtonPrimary;
