import React from 'react';

import ButtonPrimary from './ButtonPrimary.jsx';
import ButtonWarning from './ButtonWarning.jsx';

import AbstractComponent from './AbstractComponent.jsx';

class WelcomeScreen extends AbstractComponent {
  invite() {
    this.props.router.navigate('/content/inner/invite');
  }

  applyForJoin() {
    this.props.router.navigate('/content/inner/apply');
  }

  render() {
    return (
      <div>
        <div className="col-lg-2 col-lg-offset-1">
          <img src="img/logo.svg" alt="That Workshop Corporation" />
        </div>
        <div className="col-lg-6">
          <h1 dangerouslySetInnerHTML={{__html: this.props.title}}></h1>
          <h2 dangerouslySetInnerHTML={{__html: this.props.heading}}></h2>
          <div dangerouslySetInnerHTML={{__html: this.props.body}}></div>
          <div className="actions">
            <ButtonPrimary className="btn-lg" action={this.invite.bind(this)}>Enter invitation code</ButtonPrimary>
            <span>or</span>
            <ButtonWarning className="btn-lg" action={this.applyForJoin.bind(this)}>Apply for join</ButtonWarning>
          </div>
        </div>
      </div>
    );
  }
}

export default WelcomeScreen;
