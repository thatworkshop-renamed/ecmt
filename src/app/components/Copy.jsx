import React from 'react';

import AbstractComponent from './AbstractComponent.jsx';

class Copy extends AbstractComponent {
  render() {
    return <p className="copy">EVE Online&reg; and CCP&reg; and all related logos and other elements are trademarks of CCP hf.</p>;
  }
}

export default Copy;
