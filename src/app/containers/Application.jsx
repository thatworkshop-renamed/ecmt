import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {invokeContext} from './../actions';

import AbstractComponent from './../components/AbstractComponent.jsx';

class Application extends AbstractComponent {
  getLayout() {
    // TODO: add default layout configuration
    return this.resolve('/layout');
  }

  render() {
    return <div>{this.getLayout()}</div>;
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({invokeContext}, dispatch);

export default connect(state => state, mapDispatchToProps)(Application);
