import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ButtonPrimary from './../components/ButtonPrimary.jsx';
import ButtonWarning from './../components/ButtonWarning.jsx';

import {switchContext} from './../actions';

class HomeScreen extends React.Component {
  signup() {
    this.props.router.navigate('home.signup');
  }

  render() {
    return (
      <div>
        <div className="conteiner-fluid page-home">
          <div className="row header">
            <div className="col-lg-12">
              <div className="pull-right signin">
                <p>Already a member?</p>
                <button className="btn btn-primary">Sing In</button>
              </div>
            </div>
          </div>
          <div className="row body">
            <div className="col-lg-2 col-lg-offset-1">
              <img src="img/logo.svg" alt="That Workshop Corporation" />
            </div>
            <div className="col-lg-6">
              <h1>That<br />Workshop</h1>
              <h2>Welcome to the official corporation portal</h2>
              <p>That Workshop is a PvE/PvP corporation focused on the factional warfare, mining, manufaturing, research and development. We are living in a high-sec space and looking for the pilots.</p>
              <p>We are Gallente addicted and push all our forces to guard the Gallente space. Our pilots get shares, salaries, bonuese as long as a training courses. We need you now!</p>
              <div className="actions">
                <ButtonPrimary className="btn-lg" action={this.signup.bind(this)}>Enter invitation code</ButtonPrimary>
                <span>or</span>
                <ButtonWarning className="btn-lg">Apply for join</ButtonWarning>
              </div>
            </div>
          </div>
          <div className="row footer">
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({switchContext}, dispatch);

export default connect(state => state, mapDispatchToProps)(HomeScreen);
