export {combineReducers} from 'redux';

export const createReducer = (initialState, handlers) => {
  return (state = initialState, action) => {
    if (handlers.hasOwnProperty(action.type)) {
      return handlers[action.type](state, action);
    } else {
      return state;
    }
  }
};

export const invokeReducers = (reducers, state, action) => {
  const newState = {};

  for (let key in reducers) {
    newState[key] = reducers[key](state, action);
  }

  return newState;
};
