const inContext = (parent, child) => child.search(parent) === 0;

export default inContext;
