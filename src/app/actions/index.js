export const bootstrap = () => ({
  type: 'BOOTSTRAP'
});

export const switchContext = (context, payload) => ({
  type: 'SWITCH_CONTEXT',
  context,
  payload
});

export const invokeContext = (context, payload = {}, override) => ({
  type: 'INVOKE_CONTEXT',
  context,
  payload,
  override
});

export const revokeContext = (context) => ({
  type: 'REVOKE_CONTEXT',
  context
});

export const contextQueueUpdate = (queue) => ({
  type: 'CONTEXT_QUEUE_UPDATE',
  queue
});

export const routerInit = router => ({
  type: 'ROUTER_INIT',
  router
});
