import routesConfig from './../conf/routes.js';

// Cached regular expressions for matching named param parts and splatted
// parts of route strings.
const optionalParam = /\((.*?)\)/g;
const namedParam    = /(\(\?)?:\w+/g;
const escapeRegExp  = /[\-{}\[\]+?.,\\\^$|#\s]/g;

// Convert a route string into a regular expression, suitable for matching
// against the current location.
const routeToRegExp = route => {
  route = route.replace(escapeRegExp, '\\$&')
               .replace(optionalParam, '(?:$1)?')
               .replace(namedParam, (match, optional) => {
                 return optional ? match : '([^/?]+)';
               });

  return new RegExp('^' + route + '(?:\\?([\\s\\S]*))?$');
}

const routes = new Map();

for (let context in routesConfig) {
  routes.set(context, {
    path: routesConfig[context],
    regExp: routeToRegExp(routesConfig[context])
  });
}

export default routes;
