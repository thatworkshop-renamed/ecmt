import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const AgentSchema = new Schema({
  agentType: {type: Schema.Types.ObjectId, ref: 'AgentType'},
  corporation: {type: Schema.Types.ObjectId, ref: 'Corporation'},
  division: {type: Schema.Types.ObjectId, ref: 'Division'},
  location: {type: Schema.Types.ObjectId, ref: 'Location'},
  isLocator: Schema.Types.Boolean,
  level: Schema.Types.Number,
  quality: Schema.Types.Number
});

const AgentModel = mongoose.model('Agent', AgentSchema, 'agtAgents');

export default AgentModel;
