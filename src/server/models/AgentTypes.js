import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const AgentTypeSchema = new Schema({
  agentType: Schema.Types.String
});

const AgentTypeModel = mongoose.model('AgentType', AgentSchema, 'agtAgentTypes');

export default AgentTypeModel;
