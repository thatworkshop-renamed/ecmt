import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const StrategySchema = new Schema({
  title: {type: String}
});

const StrategyModel = mongoose.model('Strategy', EventSchema, 'strategies');

export default StrategyModel;
