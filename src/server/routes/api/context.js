export default app => {
  app.get('/api/static/welcome', (req, res) => {
    res.json({
      title: 'That Workshop',
      heading: 'Welcome to the official corporation portal',
      body: `
        <p>That Workshop is a PvE/PvP corporation focused on the factional warfare, mining, manufaturing, research and development. We are living in a high-sec space and looking for the pilots.</p>
        <p>We are Gallente addicted and push all our forces to guard the Gallente space. Our pilots get shares, salaries, bonuese as long as a training courses. We need you now!</p>
      `
    });
  });
};
