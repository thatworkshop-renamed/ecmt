import home from './home';
import context from './api/context';

export default app => {
  home(app);
  context(app);
}
