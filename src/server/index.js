import express from 'express';
import modRewrite from 'connect-modrewrite';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

import routes from './routes';

const app = express();

app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/ecmt');

const db = mongoose.connection;

db.on('error', console.log.bind(console, 'MongoDB connection error:'));

db.once('open', function() {
  console.log('Connected to MongoDB server.');
});

routes(app);

app.use(modRewrite([
  '^/css/(.*)$ - [L]',
  '^/img/(.*)$ - [L]',
  '^/js/(.*)$ - [L]',
  '^/fonts/(.*)$ - [L]',
  '^/lib/(.*)$ - [L]',
  '^/.*$ /index.html'
]));

app.use(express.static(__dirname + '/../../dist/public'));

app.listen(8080, function() {
  console.log('ECMT application listening on port 8080!');
});
