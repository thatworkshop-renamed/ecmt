const collections = {
  agtAgents: {
    key: 'agentID'
  },
  agtAgentTypes: {
    key: 'agentTypeID'
  },
  agtResearchAgents: {
    key: undefined
  },
  chrAncestries: {
    key: 'ancestryID'
  },
  chrAttributes: {
    key: 'attributeID'
  },
  chrBloodlines: {
    key: 'bloodlineID'
  },
  chrFactions: {
    key: 'factionID'
  },
  chrRaces: {
    key: 'raceID'
  },
  crpActivities: {
    key: 'activityID'
  },
  crpNPCCorporationDivisions: {
    key: undefined
  },
  crpNPCCorporationResearchFields: {
    key: undefined
  },
  crpNPCCorporations: {
    key: 'corporationID'
  },
  crpNPCCorporationTrades: {
    key: undefined
  },
  crpNPCDivisions: {
    key: 'divisionID'
  },
  dgmAttributeCategories: {
    key: 'categoryID'
  },
  dgmAttributeTypes: {
    key: 'attributeID'
  },
  dgmEffects: {
    key: 'effectID'
  },
  dgmExpressions: {
    key: 'expressionID'
  },
  dgmTypeAttributes: {
    key: undefined
  },
  dgmTypeEffects: {
    key: undefined
  },
  eveUnits: {
    key: 'unitID'
  },
  invContrabandTypes: {
    key: undefined
  }
  ,
  invControlTowerResourcePurposes: {
    key: 'purpose'
  },
  invControlTowerResources: {
    key: undefined
  },
  invFlags: {
    key: 'flagID'
  },

};
