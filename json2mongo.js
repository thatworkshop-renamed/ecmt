import fs from 'fs';
import mongoose from 'mongoose';

mongoose.connect('mongodb://localhost/eve');

const db = mongoose.connection;

db.on('error', console.log.bind(console, 'MongoDB connection error:'));

db.once('open', function() {
  console.log('Connected to MongoDB server.');

  const collection = new mongoose.Collection('typeIDs', db);

  const obj = JSON.parse(fs.readFileSync('./data/json/sde/fsd/typeIDs.json', 'utf8'));

  for (let key in obj) {
    collection.insert({...obj[key], _id: key});
  }

  console.log('Done.');
});
