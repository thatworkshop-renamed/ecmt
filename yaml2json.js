import yaml from 'js-yaml';
import fs from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';

fs.readdir('./data/sde/bsd', (err, files) => {
  files.forEach(file => {
    if (file === 'universe' || file === 'landmarks') return;

    mkdirp('./data/json/sde/bsd', err => {
      if (err) {
        console.log(err);
      } else {
        fs.writeFile('./data/json/sde/bsd/' + path.basename(file, '.yaml') + '.json', JSON.stringify(yaml.safeLoad(fs.readFileSync('./data/sde/bsd/' + file))), err => {
          if (err) {
            console.log(err);
          } else {
            console.log('File parsed successfully: ', file);
          }
        });
      }
    });
  });
});
